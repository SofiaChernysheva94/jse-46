package ru.t1.chernysheva.tm;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.t1.chernysheva.tm.api.endpoint.IAuthEndpoint;
import ru.t1.chernysheva.tm.api.endpoint.IDomainEndpoint;
import ru.t1.chernysheva.tm.api.service.IPropertyService;
import ru.t1.chernysheva.tm.dto.request.*;
import ru.t1.chernysheva.tm.dto.request.data.*;
import ru.t1.chernysheva.tm.dto.request.user.UserLoginRequest;
import ru.t1.chernysheva.tm.marker.SoapCategory;
import ru.t1.chernysheva.tm.service.PropertyService;

@Category(SoapCategory.class)
public class DomainEndpointTest {

    @NotNull
    private static final IPropertyService propertyService = new PropertyService();

    @NotNull
    private static final IAuthEndpoint authEndpointClient = IAuthEndpoint.newInstance(propertyService);

    @NotNull
    private static final IDomainEndpoint domainEndpointClient = IDomainEndpoint.newInstance(propertyService);

    @Nullable
    private static String adminToken;

    @BeforeClass
    public static void setUp() {
        @NotNull final UserLoginRequest loginRequest = new UserLoginRequest();
        loginRequest.setLogin(propertyService.getAdminLogin());
        loginRequest.setPassword(propertyService.getAdminPassword());
        adminToken = authEndpointClient.login(loginRequest).getToken();
    }

    @Test
    public void loadDataBackup() {
        @NotNull final DataLoadBackupRequest request = new DataLoadBackupRequest(adminToken);
        Assert.assertNotNull(domainEndpointClient.loadDataBackup(request));
    }

    @Test
    public void saveDataBackup() {
        @NotNull final DataSaveBackupRequest request = new DataSaveBackupRequest(adminToken);
        Assert.assertNotNull(domainEndpointClient.saveDataBackup(request));
    }

    @Test
    public void loadDataBase64() {
        @NotNull final DataLoadBase64Request request = new DataLoadBase64Request(adminToken);
        Assert.assertNotNull(domainEndpointClient.loadDataBase64(request));
    }

    @Test
    public void saveDataBase64() {
        @NotNull final DataSaveBase64Request request = new DataSaveBase64Request(adminToken);
        Assert.assertNotNull(domainEndpointClient.saveDataBase64(request));
    }

    @Test
    public void loadDataBinary() {
        @NotNull final DataLoadBinaryRequest request = new DataLoadBinaryRequest(adminToken);
        Assert.assertNotNull(domainEndpointClient.loadDataBinary(request));
    }

    @Test
    public void saveDataBinary() {
        @NotNull final DataSaveBinaryRequest request = new DataSaveBinaryRequest(adminToken);
        Assert.assertNotNull(domainEndpointClient.saveDataBinary(request));
    }

    @Test
    public void loadDataJsonFasterXml() {
        @NotNull final DataLoadJsonFasterXmlRequest request = new DataLoadJsonFasterXmlRequest(adminToken);
        Assert.assertNotNull(domainEndpointClient.loadDataJsonFasterXml(request));
    }

    @Test
    public void saveDataJsonFasterXml() {
        @NotNull final DataSaveJsonFasterXmlRequest request = new DataSaveJsonFasterXmlRequest(adminToken);
        Assert.assertNotNull(domainEndpointClient.saveDataJsonFasterXml(request));
    }

    @Test
    public void loadDataJsonJaxB() {
        @NotNull final DataLoadJsonJaxBRequest request = new DataLoadJsonJaxBRequest(adminToken);
        Assert.assertNotNull(domainEndpointClient.loadDataJsonJaxB(request));
    }

    @Test
    public void saveDataJsonJaxB() {
        @NotNull final DataSaveJsonJaxBRequest request = new DataSaveJsonJaxBRequest(adminToken);
        Assert.assertNotNull(domainEndpointClient.saveDataJsonJaxB(request));
    }

    @Test
    public void loadDataXmlFasterXml() {
        @NotNull final DataLoadXmlFasterXmlRequest request = new DataLoadXmlFasterXmlRequest(adminToken);
        Assert.assertNotNull(domainEndpointClient.loadDataXmlFasterXml(request));
    }

    @Test
    public void saveDataXmlFasterXml() {
        @NotNull final DataSaveXmlFasterXmlRequest request = new DataSaveXmlFasterXmlRequest(adminToken);
        Assert.assertNotNull(domainEndpointClient.saveDataXmlFasterXml(request));
    }

    @Test
    public void loadDataXmlJaxB() {
        @NotNull final DataLoadXmlJaxBRequest request = new DataLoadXmlJaxBRequest(adminToken);
        Assert.assertNotNull(domainEndpointClient.loadDataXmlJaxB(request));
    }

    @Test
    public void saveDataXmlJaxB() {
        @NotNull final DataSaveXmlJaxBRequest request = new DataSaveXmlJaxBRequest(adminToken);
        Assert.assertNotNull(domainEndpointClient.saveDataXmlJaxB(request));
    }

    @Test
    public void loadDataYamlFasterXml() {
        @NotNull final DataLoadYamlFasterXmlRequest request = new DataLoadYamlFasterXmlRequest(adminToken);
        Assert.assertNotNull(domainEndpointClient.loadDataYamlFasterXml(request));
    }

    @Test
    public void saveDataYamlFasterXml() {
        @NotNull final DataSaveYamlFasterXmlRequest request = new DataSaveYamlFasterXmlRequest(adminToken);
        Assert.assertNotNull(domainEndpointClient.saveDataYamlFasterXml(request));
    }

}

