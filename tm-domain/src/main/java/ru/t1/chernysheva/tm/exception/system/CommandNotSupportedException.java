package ru.t1.chernysheva.tm.exception.system;

import ru.t1.chernysheva.tm.exception.AbstractException;

public class CommandNotSupportedException extends AbstractException {

    public CommandNotSupportedException() {
        super("Error! Command is incorrect...");
    }

    public CommandNotSupportedException(final String command) {
        super("Error! Command ``" + command + "`` not supported");
    }

}
